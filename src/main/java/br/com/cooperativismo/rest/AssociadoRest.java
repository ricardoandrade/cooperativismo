package br.com.cooperativismo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cooperativismo.dominio.AssociadoServico;
import br.com.cooperativismo.dominio.entidade.Associado;
import br.com.cooperativismo.dominio.exception.NegocioException;
import br.com.cooperativismo.dominio.exception.ServicoExternoException;

@RestController
@RequestMapping("/cooperativismo")
public class AssociadoRest {

	@Autowired
	private AssociadoServico associadoServico;

	@PostMapping("/associado/cadastrar")
	public ResponseEntity<String> cadastrar(@RequestBody Associado associado)
			throws NegocioException, ServicoExternoException {

		associado.setCpf(retirarMascaraCPF(associado.getCpf()));

		Associado a = associadoServico.cadastrarAssociado(associado);

		return ResponseEntity.status(HttpStatus.OK).body("Cadastro realizado com sucesso! CPF: " + a.getCpf());
	}

	private String retirarMascaraCPF(String cpf) {
		return cpf.replaceAll("[^\\d]", "");
	}
}
