package br.com.cooperativismo.rest;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.cooperativismo.dominio.AssociadoServico;
import br.com.cooperativismo.dominio.PautaServico;
import br.com.cooperativismo.dominio.SessaoApp;
import br.com.cooperativismo.dominio.VotacaoServico;
import br.com.cooperativismo.dominio.VotoEnum;
import br.com.cooperativismo.dominio.entidade.Associado;
import br.com.cooperativismo.dominio.entidade.Pauta;
import br.com.cooperativismo.dominio.exception.InfraException;
import br.com.cooperativismo.dominio.exception.NegocioException;
import br.com.cooperativismo.dominio.exception.ServicoExternoException;

@RestController
@RequestMapping("/cooperativismo")
public class VotacaoRest {

	@Autowired
	private AssociadoServico associadoServico;

	@Autowired
	private PautaServico pautaServico;

	@Autowired
	private VotacaoServico votacaoServico;

	@Autowired
	private SessaoApp sessaoApp;

	@GetMapping("/votacao/abertura")
	public ResponseEntity<String> abrirVotacao(@RequestParam(name = "cod_pauta") Long codPauta,
			@RequestParam(name = "cpf") String cpf, HttpSession httpSession)
			throws NegocioException, InfraException, ServicoExternoException {

		httpSession.setMaxInactiveInterval(60);

		HttpSession session = sessaoApp.getHttpSession();

		Associado associado = associadoServico.validarCPF(cpf);
		Pauta pauta = pautaServico.validarPauta(codPauta);
		votacaoServico.validarVoto(cpf, codPauta);

		session.setAttribute("associado", associado);
		session.setAttribute("pauta", pauta);

		return ResponseEntity.status(HttpStatus.OK)
				.body("Votação Aberta!\n\r Título:" + pauta.getTitulo() + "\n\r " + pauta.getDescricao());
	}

	@GetMapping("/votacao")
	public ResponseEntity<String> votar(@RequestParam(name = "voto") String voto)
			throws NegocioException, InfraException {

		HttpSession session = sessaoApp.getHttpSession();

		Associado associado = (Associado) session.getAttribute("associado");
		Pauta pauta = (Pauta) session.getAttribute("pauta");

		VotoEnum votoEnum = VotoEnum.getVotoPorNome(voto);

		votacaoServico.cadastrarVoto(associado, pauta, votoEnum);

		session.invalidate();

		return ResponseEntity.status(HttpStatus.OK).body("O Voto foi Computado!");
	}

	@GetMapping("/votacao/resultado")
	public ResponseEntity<String> resultadoVotacao(@RequestParam(name = "pauta") Long pauta) throws NegocioException {

		Map<String, Integer> mapVoto = votacaoServico.retornarResultado(pauta);

		String msg = "Total de votos -> SIM: " + mapVoto.get(VotoEnum.SIM.toString()) + " NÃO: "
				+ mapVoto.get(VotoEnum.NAO.toString());

		return ResponseEntity.status(HttpStatus.OK).body(msg);
	}

}
