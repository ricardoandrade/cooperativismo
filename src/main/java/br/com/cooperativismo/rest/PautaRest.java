package br.com.cooperativismo.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cooperativismo.dominio.PautaServico;
import br.com.cooperativismo.dominio.entidade.Pauta;


@RestController
@RequestMapping("/cooperativismo")
public class PautaRest {

	@Autowired
	private PautaServico pautaServico;
	
	@PostMapping("/pauta/cadastrar")
	public  ResponseEntity<String> cadastrar(@RequestBody Pauta pauta) {
		Pauta p = pautaServico.cadastrarPauta(pauta);
		 
		return ResponseEntity.status(HttpStatus.OK).body("Cadastro realizado com sucesso! Código da pauta = "+p.getCod());
	}
}
