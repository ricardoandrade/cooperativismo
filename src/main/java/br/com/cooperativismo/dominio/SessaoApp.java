package br.com.cooperativismo.dominio;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import br.com.cooperativismo.dominio.exception.InfraException;

@Component
public class SessaoApp {
	
	private HttpSession httpSession;
	
	public HttpSession getHttpSession() throws InfraException {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		httpSession = attr.getRequest().getSession(false);
		
		if (ObjectUtils.isEmpty(httpSession)) {
			throw new InfraException("A sessão não foi criada ou foi fechada por falta de atividade");
		}
		
		return httpSession;
	}
	
	
	
}
