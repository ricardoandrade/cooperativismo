package br.com.cooperativismo.dominio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import br.com.cooperativismo.dominio.entidade.Associado;
import br.com.cooperativismo.dominio.entidade.AssociadoRepository;
import br.com.cooperativismo.dominio.exception.NegocioException;
import br.com.cooperativismo.dominio.exception.ServicoExternoException;
import br.com.cooperativismo.infra.ChamadaServicoCPF;

@Component
public class AssociadoServico {

	@Autowired
	private AssociadoRepository associadoRepository;

	@Autowired
	private ChamadaServicoCPF chamadaServicoCPF;

	public Associado cadastrarAssociado(Associado associado) throws NegocioException, ServicoExternoException {
		
		Associado a = buscarPorCPF(associado.getCpf());

		if (!ObjectUtils.isEmpty(a)) {
			throw new NegocioException("O associado já existe na base. Informe outro CPF");
		}

		chamadaServicoCPF.validacaoExternaCPF(associado.getCpf());
		
		return associadoRepository.save(associado);
	}

	public Associado buscarPorCPF(String cpf) {

		return associadoRepository.buscarPorCPF(cpf);
	}

	public Associado buscarPorCod(Long cod) {
		return associadoRepository.findById(cod).orElse(null);
	}

	public Associado validarCPF(String cpf) throws NegocioException, ServicoExternoException {
		Associado associado = buscarPorCPF(cpf);
		if (ObjectUtils.isEmpty(associado)) {
			throw new NegocioException("O CPF não existe na base.");
		}

	//	if (!isCpfHabilitado(cpf)) {
		//	throw new NegocioException("O CPF não está habilitado");
	//	}

		return associado;
	}

	public boolean isAssociadoExisteNaBase(String cpf) {
		return !ObjectUtils.isEmpty(buscarPorCPF(cpf));
	}

	public boolean isCpfHabilitado(String cpf) throws ServicoExternoException {
		ResponseEntity<SituacaoCPF> response = chamadaServicoCPF.validacaoExternaCPF(cpf);
		return response.getBody().isCpfHabilitado();
	}
}
