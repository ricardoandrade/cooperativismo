package br.com.cooperativismo.dominio;

import org.springframework.http.HttpStatus;

public class ResultadoErro {

	private Integer httpStatus;
	
	private String erro;
	
	private String mensagem;
	

	public ResultadoErro(HttpStatus status, String erro, String message) {
		
		this.httpStatus = status.value();
		this.erro = erro;
		this.mensagem = message;
	}



	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}



	public Integer getHttpStatus() {
		return httpStatus;
	}



	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

	
}
