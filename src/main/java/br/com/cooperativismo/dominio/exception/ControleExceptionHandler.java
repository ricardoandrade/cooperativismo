package br.com.cooperativismo.dominio.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.cooperativismo.dominio.ResultadoErro;

@ControllerAdvice
public class ControleExceptionHandler  {

	@ExceptionHandler(NegocioException.class)
	public ResponseEntity<ResultadoErro> erroNegocio(NegocioException ex, HttpServletRequest req){
		ResultadoErro erro = new ResultadoErro(HttpStatus.BAD_REQUEST,"Regra de Negócio",ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
	}
	
	@ExceptionHandler(InfraException.class)
	public ResponseEntity<ResultadoErro> erroInfra(InfraException ex, HttpServletRequest req){
		ResultadoErro erro = new ResultadoErro(HttpStatus.REQUEST_TIMEOUT,"Regra de Infra",ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
	}
	
	@ExceptionHandler(ServicoExternoException.class)
	public ResponseEntity<ResultadoErro> erroServicoExterno(ServicoExternoException ex, HttpServletRequest req){
		ResultadoErro erro = new ResultadoErro(HttpStatus.NOT_FOUND,"Chamada Externa",ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
}
