package br.com.cooperativismo.dominio.exception;

public class InfraException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5992482359975938899L;

	public InfraException(String errorMessage) {
		super(errorMessage);
	}
}
