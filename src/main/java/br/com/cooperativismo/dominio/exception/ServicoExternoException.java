package br.com.cooperativismo.dominio.exception;

public class ServicoExternoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7393877207315450582L;
	
	 public ServicoExternoException(String errorMessage) {
	        super(errorMessage);
	    }

}
