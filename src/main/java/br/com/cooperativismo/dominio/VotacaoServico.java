package br.com.cooperativismo.dominio;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import br.com.cooperativismo.dominio.entidade.Associado;
import br.com.cooperativismo.dominio.entidade.Pauta;
import br.com.cooperativismo.dominio.entidade.ResultadoVotacao;
import br.com.cooperativismo.dominio.entidade.Votacao;
import br.com.cooperativismo.dominio.entidade.VotacaoRepository;
import br.com.cooperativismo.dominio.exception.NegocioException;

@Component
public class VotacaoServico {

	@Autowired
	private VotacaoRepository votacaoRepository;
	
	public Votacao cadastrarVoto(Associado associado, Pauta pauta, VotoEnum votoEnum) throws NegocioException {
		if (votoEnum == null) {
			throw new NegocioException("Voto Inválido! Use SIM ou NAO para votar");
		}
		
		Votacao votacao = new Votacao();
		
		votacao.setAssociado(associado);
		votacao.setPauta(pauta);
		votacao.setVoto(votoEnum.getVotoNum());
		votacao.setDtCricao(new Date());
		votacaoRepository.save(votacao);
		
		return null;
	}

	public List<ResultadoVotacao> listarResultado(Long pauta) throws NegocioException {
		List<ResultadoVotacao> resultado = votacaoRepository.listarResultado(pauta);
		
		if (ObjectUtils.isEmpty(resultado)) {
			throw new NegocioException("Não foi possível retornar o resultado da votação");
		}
		return 	resultado;
	}
	
	public Map<String, Integer> retornarResultado(Long pauta) throws NegocioException {
		List<ResultadoVotacao> resultado = listarResultado(pauta);

		Map<String, Integer> mapVoto = new HashMap<>();
		mapVoto.put(VotoEnum.SIM.toString(), 0);
		mapVoto.put(VotoEnum.NAO.toString(), 0);
		resultado.stream().forEach(voto -> montarResultado(voto, mapVoto));
		
		return mapVoto;
	}
	
	private void montarResultado(ResultadoVotacao resultado, Map<String, Integer> mapVoto) {
		VotoEnum votoEnum = VotoEnum.getVotoPorNumero(resultado.getVoto());
		mapVoto.put(votoEnum.getVotoString(), resultado.getContador());
	}

	public void validarVoto(String cpf, Long codPauta) throws NegocioException {
		Long cod = votacaoRepository.buscarVoto(cpf,codPauta);
		
		if (!ObjectUtils.isEmpty(cod)) {
			throw new NegocioException("Voto Inválido! Um associado só pode votar 1 (uma) vez para cada pauta");
		}
	}
}
