package br.com.cooperativismo.dominio;

public enum VotoEnum {

	SIM(1,"SIM"),
	NAO(0,"NAO");
	
	private Integer votoNum;
	
	private String votoString;
	
	 VotoEnum(Integer votoNum, String str) {
		 	this.votoNum = votoNum;
		 	this.votoString = str;
	}
	 
	 public Integer getVotoNum() {
		 return this.votoNum;
	 }

	public String getVotoString() {
		return votoString;
	}

	public  static VotoEnum getVotoPorNome(String nome) {
		VotoEnum numero = null;
		for ( VotoEnum voto : values()) {
			if (voto.getVotoString().equalsIgnoreCase(nome)) {
				numero = voto;
			}
		}
		
		return numero;
	}
	
	public  static VotoEnum getVotoPorNumero(Integer v) {
		VotoEnum nome = null;
		for ( VotoEnum voto : values()) {
			if (voto.getVotoNum() == v) {
				nome = voto;
			}
		}
		
		return nome;
	}
	
}
