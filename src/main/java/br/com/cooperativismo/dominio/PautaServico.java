package br.com.cooperativismo.dominio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import br.com.cooperativismo.dominio.entidade.Pauta;
import br.com.cooperativismo.dominio.entidade.PautaRepository;
import br.com.cooperativismo.dominio.exception.NegocioException;

@Component
public class PautaServico {

	@Autowired
	private PautaRepository pautaRepository;

	public Pauta cadastrarPauta(Pauta pauta) {
		return pautaRepository.save(pauta);
	}

	public Pauta buscarPauta(Long cod) {
		return pautaRepository.findById(cod).orElse(null);
	}
	
	public Pauta validarPauta(Long cod) throws NegocioException {
		
		Pauta pauta = buscarPauta(cod);
		
		if(ObjectUtils.isEmpty(pauta)) {
			throw new NegocioException("A pauta não existe na base.");
		}
		
		return pauta;
	}

}
