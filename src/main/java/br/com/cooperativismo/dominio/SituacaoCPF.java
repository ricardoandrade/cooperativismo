package br.com.cooperativismo.dominio;

public class SituacaoCPF {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public boolean isCpfHabilitado() {
		return (status.equals("ABLE_TO_VOTE"));
	}
	
}
