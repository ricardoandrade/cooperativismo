package br.com.cooperativismo.dominio.entidade;

public interface ResultadoVotacao {

	
	Integer getVoto();
	
	Integer getContador();
	
}
