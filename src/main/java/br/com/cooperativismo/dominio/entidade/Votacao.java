package br.com.cooperativismo.dominio.entidade;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_votacao", schema = "cooperativismo")
public class Votacao {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cod;

	@ManyToOne
	@JoinColumn(name = "cod_associado",nullable = false)
	private Associado associado;
	
	@ManyToOne
	@JoinColumn(name = "cod_pauta",nullable = false)
	private Pauta pauta;
	
	private Integer voto;
	
	@Column(name="dt_criacao")
	private Date dtCricao;

	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

	public Associado getAssociado() {
		return associado;
	}

	public void setAssociado(Associado associado) {
		this.associado = associado;
	}

	public Pauta getPauta() {
		return pauta;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}

	public Integer getVoto() {
		return voto;
	}

	public void setVoto(Integer voto) {
		this.voto = voto;
	}

	public Date getDtCricao() {
		return dtCricao;
	}

	public void setDtCricao(Date dtCricao) {
		this.dtCricao = dtCricao;
	}
}
