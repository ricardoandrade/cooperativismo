package br.com.cooperativismo.dominio.entidade;

public class ConstantesQueries {

	public static final String RESULTADO_VOTACAO_SQL = "select voto, count(voto) as contador "
			+ " from cooperativismo.tb_votacao "
			+ " where cod_pauta = :pauta"
			+ " group by voto";
}
