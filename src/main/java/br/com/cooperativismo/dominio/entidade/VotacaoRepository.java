package br.com.cooperativismo.dominio.entidade;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VotacaoRepository extends JpaRepository<Votacao, Long> {

	@Query(value = ConstantesQueries.RESULTADO_VOTACAO_SQL, nativeQuery = true)
	List<ResultadoVotacao> listarResultado(@Param("pauta") Long pauta);

	@Query("select vo.cod from Votacao vo INNER JOIN vo.associado ass where vo.pauta.cod = :cod_pauta AND ass.cpf = :cpf ")
	Long buscarVoto(@Param("cpf") String cpf, @Param("cod_pauta") Long codPauta);

}
