package br.com.cooperativismo.infra;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.cooperativismo.dominio.SituacaoCPF;
import br.com.cooperativismo.dominio.exception.ServicoExternoException;

@Component
public class ChamadaServicoCPF {

	
	public ResponseEntity<SituacaoCPF> validacaoExternaCPF(String cpf) throws ServicoExternoException {
		ResponseEntity<SituacaoCPF> response = null;
		String uri = "https://user-info.herokuapp.com/users/" + cpf;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", MediaType.APPLICATION_JSON + "; charset=utf-8");
		HttpEntity<Object> entity = new HttpEntity<Object>(httpHeaders);
		try {
			response = restTemplate.exchange(uri, HttpMethod.GET, entity, SituacaoCPF.class);
		} catch (HttpClientErrorException e) {
			e.getMessage();
			throw new ServicoExternoException("O CPF é inválido");
		}
		
		return response;
	}
}
