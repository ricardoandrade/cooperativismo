# cooperativismo
Projeto de votação do Associado

OBS: Para os passos de configuração a base de dados do projeto, leia o arquivo README na raiz do projeto


# ENDPOINT DO SISTEMA
## Cadastro Associado:
POST- http://localhost:8080/cooperativismo/associado/cadastrar
    
	{
    "cpf":"123.456.789-10",
    "nome":"usuario"
    }

## Cadastro Pauta:
POST- http://localhost:8080/cooperativismo/pauta/cadastrar

{
    "titulo":"pauta 1",
    "descricao":"teste descricao"
}

## Abrir sessão de votação:
GET- http://localhost:8080/cooperativismo/votacao/abertura?cod_pauta=1&cpf=12345678910

## Votação:
GET- http://localhost:8080/cooperativismo/votacao?voto=SIM

## Resultado:
GET- http://localhost:8080/votacao/resultado?pauta=1


teste